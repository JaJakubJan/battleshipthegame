package controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;

import java.net.URL;
import java.util.ResourceBundle;

public class GameOptionController implements Initializable {
    private MainController mainController;

    @FXML
    ChoiceBox diffChoiceBox;

    @FXML
    private void singleplayerOnAction() {
        mainController.resizeAndReallocWindow(MainController.BIG_WINDOW_PARAMETERS);
        if (diffChoiceBox.getValue().toString().compareTo("Easy") == 0)
            mainController.setBoardGame('E');
        else if (diffChoiceBox.getValue().toString().compareTo("Regular") == 0)
            mainController.setBoardGame('R');
        else
            mainController.setBoardGame('H');
    }

    @FXML
    private void multiplayerOnAction() {
        mainController.resizeAndReallocWindow(MainController.BIG_WINDOW_PARAMETERS);
        mainController.setBoardGame('M');
    }

    @FXML
    private void backOnAction() {
        mainController.setMenu();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        diffChoiceBox.getItems().addAll("Easy", "Regular", "Hard");
        diffChoiceBox.getSelectionModel().select(1);
    }

    void setMainController(MainController mainController) {
        this.mainController = mainController;
    }
}
