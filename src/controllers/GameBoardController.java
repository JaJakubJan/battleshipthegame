package controllers;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import main.Field;
import main.IOFileMediatesClass;
import main.Ship;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class GameBoardController implements Initializable {
    private final int NUMBER_OF_BATTLESHIPS = 2;
    private final int NUMBER_OF_CRUISERS = 3;
    private final int NUMBER_OF_DESTROYERS = 3;
    private final int NUMBER_OF_TORPEDO_BOATS = 2;
    private final int HARD_AI_PERCENTAGE_CHANCE_TO_AUTOHIT = 80;
    private final int FIELD_BUTTON_SIZE_IN_PIXELS = 40;
    private final int NUMBER_OF_SHIPS = NUMBER_OF_BATTLESHIPS + NUMBER_OF_CRUISERS + NUMBER_OF_DESTROYERS + NUMBER_OF_TORPEDO_BOATS;

    private MainController mainController;
    private char type;
    private Button[][] myShipsButtons = new Button[10][10];
    private Button[][] oppositeShipsButtons = new Button[10][10];
    // true-player - first one / false-player - sec one
    private Field[][] firstPlayerFields = new Field[10][10];
    private Field[][] secondPlayerFields = new Field[10][10];
    private boolean playerTurn = true;
    private boolean isSetting = true;
    private int firstShipsToSetLeft = NUMBER_OF_SHIPS;
    private int secondShipsToSetLeft = NUMBER_OF_SHIPS;
    private boolean isFired = false;
    private boolean isGameEnded = false;
    private boolean isRandomHitted = false;
    // true - Y, false - X
    private boolean orientation = true;
    private Orientation orientationTEMP = Orientation.Y;
    // regular and difficult bot option
    private ArrayList<Integer> prevN = new ArrayList<>();
    private ArrayList<Integer> prevL = new ArrayList<>();

    @FXML
    private ImageView logoImage;
    @FXML
    private ImageView shipIcon;
    @FXML
    private GridPane oppShipsGrid;
    @FXML
    private GridPane myShipsGrid;
    @FXML
    private Label playerLabel;
    @FXML
    private Button nextTurnButton;
    @FXML
    private ListView messageList;
    @FXML
    private Label playerWinLabel;
    @FXML
    private Label rotLabel;

    @FXML
    private void randomShootOnAction() {
        randomHit();
    }

    @FXML
    private void nextOnAction() {
        checkGameStatus();
        if (!isSetting && isFired && !isGameEnded && type == 'M') {
            isFired = false;
            coverAllButtons();
            waitTheGame();
        }
        if (isGameEnded)
            addMessage("Game is ended! Please click back or quit button!");
        if (isSetting)
            setOnAction();
        if (!isSetting && isFired && !isGameEnded && type != 'M') {
            isFired = false;
            setPlayerTurn(false);
            convertAllTargetFielsdToButton(isPlayerTurn());
            if (type == 'E')
                easyComputerHit();
            else if (type == 'R')
                regularComputerHit();
            else
                hardComputerHit();
            if (!isGameEnded) {
                isFired = false;
                setPlayerTurn(true);
                coverAllButtons();
                convertAllVisableFielsdToButton(isPlayerTurn());
                convertAllTargetFielsdToButton(isPlayerTurn());
                addMessage("Shoot the target!");
            } else {
                coverAllButtons();
                convertAllVisableFielsdToButton(true);
                convertAllTargetFielsdToButton(true);
            }
        }
    }

    @FXML
    private void saveOnAction() {
        if (!isSetting) {
            if (!isGameEnded) {
                makeMessWindow(MainController.SAVING_SCREEN_LOCATION, SavingController.class, MainController.BIG_WINDOW_PARAMETERS);
                Stage gameStage = (Stage) playerLabel.getScene().getWindow();
                gameStage.hide();
            } else
                addMessage("Game is ended! Please click back or quit button!");
        } else
            addMessage("Saving disable - set your ships first!");
    }

    @FXML
    private void resetOnAction() {
        if (isSetting) {
            addMessage("Setting reset!");
            if (playerTurn) {
                resetFields(firstPlayerFields);
                firstShipsToSetLeft = NUMBER_OF_SHIPS;
            } else {
                resetFields(secondPlayerFields);
                secondShipsToSetLeft = NUMBER_OF_SHIPS;
            }
            convertAllVisableFielsdToButton(playerTurn);
        }
    }

    private void resetFields(Field[][] playerFields) {
        for (Field[] rowOfFields : playerFields) {
            for (Field field : rowOfFields) {
                field.setImageType("blank");
                field.setSetted(false);
                field.setShip(null);
            }
        }
    }

    @FXML
    private void setOnAction() {
        if (playerTurn && firstShipsToSetLeft == 0 && type == 'M' && isSetting) {
            coverAllButtons();
            waitTheGame();
        } else if (!playerTurn && secondShipsToSetLeft == 0 && type == 'M' && isSetting) {
            coverAllButtons();
            waitTheGame();
            isSetting = false;
            rotLabel.setText("");
        } else if (playerTurn && firstShipsToSetLeft == 0 && isSetting) {
            setPlayerTurn(false);
            coverAllButtons();
            randomAlocate();
            setPlayerTurn(true);
            convertAllVisableFielsdToButton(playerTurn);
            convertAllTargetFielsdToButton(playerTurn);
            isSetting = false;
            rotLabel.setText("");
        }
    }

    @FXML
    private void quitOnAction() {
        Platform.exit();
    }

    @FXML
    private void backOnAction() {
        mainController.setMenu();
        mainController.resizeAndReallocWindow(MainController.SMALL_WINDOW_PARAMETERS);
    }

    @FXML
    private void randomOnAction() {
        resetOnAction();
        randomAlocate();
    }

    private enum Orientation {
        X, Y
    }

    private void rotate() {
        orientationTEMP = (orientationTEMP.toString().compareTo("X") == 0) ? Orientation.Y : Orientation.X;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        nextTurnButton.setTextFill(Color.GREEN);
        rotLabel.setTextFill(Color.CORAL);
        playerWinLabel.setText("");
        logoImage.setImage(new Image("file:graphic/logo.png"));
        shipIcon.setImage(new Image("file:graphic/shipIcon.png"));
        for (int r = 0; r < firstPlayerFields.length; r++) {
            for (int c = 0; c < firstPlayerFields[r].length; c++) {
                final int R = r;
                final int C = c;
                firstPlayerFields[R][C] = new Field(R, C);
                secondPlayerFields[R][C] = new Field(R, C);
                Button tempButtonFirst = createDefaultButton();
                Button tempButtonSecond = createDefaultButton();
                tempButtonFirst.setOnMouseClicked(e -> {
                    MouseButton mouseButton = e.getButton();
                    if (mouseButton == MouseButton.PRIMARY)
                        myShipsLeftClick();
                    else if (mouseButton == MouseButton.SECONDARY)
                        myShipsRightClick(R, C);
                });
                tempButtonSecond.setOnMouseClicked(e -> {
                    MouseButton mouseButton = e.getButton();
                    if (mouseButton == MouseButton.PRIMARY)
                        oppShipsClick(R, C);
                });
                tempButtonFirst.setOnMouseEntered(e -> myShipsEntered(R, C));
                tempButtonSecond.setOnMouseEntered(e -> oppositeShipsEntered(R, C));
                tempButtonFirst.setOnMouseExited(e -> myShipsExited(R, C));
                tempButtonSecond.setOnMouseExited(e -> oppositeShipsExited(R, C));
                myShipsButtons[R][C] = tempButtonFirst;
                oppositeShipsButtons[R][C] = tempButtonSecond;
            }
        }
        coverAllButtons();
        updateShipsGrids();
        addMessage("Set your ships!");
    }

    private Button createDefaultButton() {
        Button button = new Button();
        button.setPadding(new Insets(0));
        button.setMinSize(FIELD_BUTTON_SIZE_IN_PIXELS, FIELD_BUTTON_SIZE_IN_PIXELS);
        return button;
    }

    private void myShipsLeftClick() {
        boolean flag = false;
        if (isSetting) {
            if (playerTurn) {
                for (int n = 0; n < 10; n++) {
                    for (int l = 0; l < 10; l++) {
                        if (!firstPlayerFields[n][l].isSetted() && firstPlayerFields[n][l].getImageType().compareTo("blank") != 0) {
                            firstPlayerFields[n][l].setSetted(true);
                            flag = true;
                        }
                    }
                }
                if (flag) {
                    if (firstShipsToSetLeft == 10)
                        addMessage("First Battleship setted up");
                    if (firstShipsToSetLeft == 9)
                        addMessage("Second Battleship setted up");
                    if (firstShipsToSetLeft == 8)
                        addMessage("First Cruiser setted up");
                    if (firstShipsToSetLeft == 7)
                        addMessage("Second Cruiser setted up");
                    if (firstShipsToSetLeft == 6)
                        addMessage("Third Cruiser setted up");
                    if (firstShipsToSetLeft == 5)
                        addMessage("First Destroyer setted up");
                    if (firstShipsToSetLeft == 4)
                        addMessage("Second Destroyer setted up");
                    if (firstShipsToSetLeft == 3)
                        addMessage("Third Destroyer setted up");
                    if (firstShipsToSetLeft == 2)
                        addMessage("First Torpedo Boat setted up");
                    if (firstShipsToSetLeft == 1)
                        addMessage("Second Torpedo Boat setted up");
                    firstShipsToSetLeft--;
                } else if (firstShipsToSetLeft != 0)
                    addMessage("Wrong positioning!");
                else
                    addMessage("Click set or reset to continue!");
            } else {
                for (int n = 0; n < 10; n++) {
                    for (int l = 0; l < 10; l++) {
                        if (!secondPlayerFields[n][l].isSetted() && secondPlayerFields[n][l].getImageType().compareTo("blank") != 0) {
                            secondPlayerFields[n][l].setSetted(true);
                            flag = true;
                        }
                    }
                }
                if (flag) {
                    if (secondShipsToSetLeft == 10)
                        addMessage("First Battleship setted up");
                    if (secondShipsToSetLeft == 9)
                        addMessage("Second Battleship setted up");
                    if (secondShipsToSetLeft == 8)
                        addMessage("First Cruiser setted up");
                    if (secondShipsToSetLeft == 7)
                        addMessage("Second Cruiser setted up");
                    if (secondShipsToSetLeft == 6)
                        addMessage("Third Cruiser setted up");
                    if (secondShipsToSetLeft == 5)
                        addMessage("First Destroyer setted up");
                    if (secondShipsToSetLeft == 4)
                        addMessage("Second Destroyer setted up");
                    if (secondShipsToSetLeft == 3)
                        addMessage("Third Destroyer setted up");
                    if (secondShipsToSetLeft == 2)
                        addMessage("First Torpedo Boat setted up");
                    if (secondShipsToSetLeft == 1)
                        addMessage("Second Torpedo Boat setted up");
                    secondShipsToSetLeft--;
                } else if (secondShipsToSetLeft != 0)
                    addMessage("Wrong positioning!");
                else
                    addMessage("Click set or reset to continue!");
            }
        } else
            addMessage("Setting is over - shoot the target!");
    }

    private void myShipsRightClick(final int N, final int L) {
        rotate();
        if (isSetting) {
            if (orientation)
                addMessage("Rotate to X");
            else
                addMessage("Rotate to Y");
            for (int n = 0; n < 10; n++) {
                for (int l = 0; l < 10; l++) {
                    if (!(N == n && L == l)) {
                        if (playerTurn && !firstPlayerFields[n][l].isSetted())
                            firstPlayerFields[n][l].setImageType("blank", null);
                        else if (!playerTurn && !secondPlayerFields[n][l].isSetted())
                            secondPlayerFields[n][l].setImageType("blank", null);
                        convertVisableFieldToButton(n, l, playerTurn);
                    }
                }
            }
            orientation = !orientation;
            myShipsEntered(N, L);
        } else
            addMessage("Setting is over - shoot the target!");
    }

    private void oppShipsClick(final int N, final int L) {
        String message;
        if (!isGameEnded) {
            if (isSetting()) {
                addMessage("Set your ships first!");
            } else if (!isFired) {
                if (playerTurn) {
                    message = secondPlayerFields[N][L].hit();
                } else {
                    message = firstPlayerFields[N][L].hit();
                }
                if (message.compareTo("Miss.. ") == 0)
                    isFired = true;
                addMessage(message);
                if (message.compareTo("Uncorrected target!") != 0) {
                    isRandomHitted = true;
                }
            } else {
                addMessage("You already shot! Click next turn!");
                isRandomHitted = true;
            }
            convertAllTargetFielsdToButton(playerTurn);
        } else {
            addMessage("Game is ended! Please click back or quit button!");
            isRandomHitted = true;
        }
        checkGameStatus();
    }

    private void myShipsEntered(final int N, final int L) {
        if (isSetting) {
            Field[][] playerFields = playerTurn ? firstPlayerFields : secondPlayerFields;
            int shipsToSetLeft = playerTurn ? firstShipsToSetLeft : secondShipsToSetLeft;
            if (shipsToSetLeft > NUMBER_OF_SHIPS - NUMBER_OF_BATTLESHIPS) {
                shipEntering(N, L, playerFields, new Ship("battleShip", 4));
            } else if (shipsToSetLeft > NUMBER_OF_SHIPS - NUMBER_OF_BATTLESHIPS - NUMBER_OF_CRUISERS) {
                shipEntering(N, L, playerFields, new Ship("cruiser", 3));
            } else if (shipsToSetLeft > NUMBER_OF_SHIPS - NUMBER_OF_BATTLESHIPS - NUMBER_OF_CRUISERS - NUMBER_OF_DESTROYERS) {
                shipEntering(N, L, playerFields, new Ship("destroyer", 2));
            } else if (shipsToSetLeft > NUMBER_OF_SHIPS - NUMBER_OF_BATTLESHIPS - NUMBER_OF_CRUISERS - NUMBER_OF_DESTROYERS - NUMBER_OF_TORPEDO_BOATS) {
                shipEntering(N, L, playerFields, new Ship("torpedoBoat", 1));
            }
            convertAllVisableFielsdToButton(playerTurn);
        }
    }

    private void shipEntering(final int N, final int L, Field[][] PlayerFields, Ship ship) {
        Ship movedShip = ship.getName().compareTo("blank") == 0 ? null : ship;
        if (checkNeighborhood(N, L, ship.getBulk())) {
            if (orientationTEMP.toString().compareTo("Y") == 0 && ship.getBulk() + L <= 10) {
                for (int i = 1; i <= ship.getBulk(); i++)
                    PlayerFields[N][L + i - 1].setImageType(ship.getName() + i + orientationTEMP, movedShip);
            } else if (orientationTEMP.toString().compareTo("X") == 0 && ship.getBulk() + N <= 10)
                for (int i = 1; i <= ship.getBulk(); i++)
                    PlayerFields[N + i - 1][L].setImageType(ship.getName() + i + orientationTEMP, movedShip);
        }
    }

    private void myShipsExited(final int N, final int L) {
            if (isSetting) {
                Field[][] playerFields = playerTurn ? firstPlayerFields : secondPlayerFields;
                int shipsToSetLeft = playerTurn ? firstShipsToSetLeft : secondShipsToSetLeft;
                if (shipsToSetLeft > NUMBER_OF_SHIPS - NUMBER_OF_BATTLESHIPS) {
                    shipExiting(N, L, playerFields, new Ship("battleShip", 4));
                } else if (shipsToSetLeft > NUMBER_OF_SHIPS - NUMBER_OF_BATTLESHIPS - NUMBER_OF_CRUISERS) {
                    shipExiting(N, L, playerFields, new Ship("cruiser", 3));
                } else if (shipsToSetLeft > NUMBER_OF_SHIPS - NUMBER_OF_BATTLESHIPS - NUMBER_OF_CRUISERS - NUMBER_OF_DESTROYERS) {
                    shipExiting(N, L, playerFields, new Ship("destroyer", 2));
                } else if (shipsToSetLeft > NUMBER_OF_SHIPS - NUMBER_OF_BATTLESHIPS - NUMBER_OF_CRUISERS - NUMBER_OF_DESTROYERS - NUMBER_OF_TORPEDO_BOATS) {
                    shipExiting(N, L, playerFields, new Ship("torpedoBoat", 1));
                }
                convertAllVisableFielsdToButton(playerTurn);
            }
    }

    private void shipExiting(final int N, final int L, Field[][] PlayerFields, Ship ship) {
        if (checkNeighborhood(N, L, ship.getBulk())) {
            if (orientationTEMP.toString().compareTo("Y") == 0 && ship.getBulk() + L <= 10) {
                for (int i = 1; i <= ship.getBulk(); i++)
                    PlayerFields[N][L + i - 1].setImageType("blank", null);
            } else if (orientationTEMP.toString().compareTo("X") == 0 && ship.getBulk() + N <= 10)
                for (int i = 1; i <= ship.getBulk(); i++)
                    PlayerFields[N + i - 1][L].setImageType("blank", null);
        }
    }

    private void oppositeShipsEntered(final int N, final int L) {
        Image image = new Image("file:graphic/target.png");
        if (playerTurn && !secondPlayerFields[N][L].isHitted()) {
            oppositeShipsButtons[N][L].setGraphic(new ImageView(image));
        } else if (!playerTurn && !firstPlayerFields[N][L].isHitted()) {
            oppositeShipsButtons[N][L].setGraphic(new ImageView(image));
        }
    }

    private void oppositeShipsExited(final int N, final int L) {
        Image image = new Image("file:graphic/blank.png");
        if (playerTurn && !secondPlayerFields[N][L].isHitted()) {
            oppositeShipsButtons[N][L].setGraphic(new ImageView(image));
        } else if (!playerTurn && !firstPlayerFields[N][L].isHitted()) {
            oppositeShipsButtons[N][L].setGraphic(new ImageView(image));
        }
    }

    private void waitTheGame() {
        messageList.getItems().clear();
        if (!isSetting())
            addMessage("Shoot the target!");
        else
            addMessage("Set your ships!");
        if (playerTurn)
            playerLabel.setText("Player 2");
        else
            playerLabel.setText("Player 1");
        makeMessWindow("/fxml/WaitingScreen.fxml", WaitingController.class, MainController.BIG_WINDOW_PARAMETERS);
        Stage stage = (Stage) playerLabel.getScene().getWindow();
        stage.hide();
    }

    private void convertVisableFieldToButton(final int N, final int L, final boolean PLAYER) {
        String location;
        if (PLAYER) {
            if (firstPlayerFields[N][L].isHitted())
                location = "file:graphic/" + firstPlayerFields[N][L].getImageType() + "Exp.png";
            else
                location = "file:graphic/" + firstPlayerFields[N][L].getImageType() + ".png";
        } else {
            if (secondPlayerFields[N][L].isHitted())
                location = "file:graphic/" + secondPlayerFields[N][L].getImageType() + "Exp.png";
            else
                location = "file:graphic/" + secondPlayerFields[N][L].getImageType() + ".png";
        }
        Image coverImage = new Image(location);
        myShipsButtons[N][L].setGraphic(new ImageView(coverImage));
    }

    public void convertAllVisableFielsdToButton(final boolean PLAYER) {
        for (int n = 0; n < 10; n++) {
            for (int l = 0; l < 10; l++) {
                convertVisableFieldToButton(n, l, PLAYER);
            }
        }
    }

    private void convertTargetFieldToButton(final int N, final int L, final boolean PLAYER) {
        String location;
        if (PLAYER) {
            if (secondPlayerFields[N][L].getImageType().compareTo("blank") == 0 || (secondPlayerFields[N][L].getShip() != null && secondPlayerFields[N][L].getShip().isDestroyed()))
                location = "file:graphic/" + secondPlayerFields[N][L].getImageType() + "rExp.png";
            else
                location = "file:graphic/hitAndBoom.png";
        } else {
            if (firstPlayerFields[N][L].getImageType().compareTo("blank") == 0 || (firstPlayerFields[N][L].getShip() != null && firstPlayerFields[N][L].getShip().isDestroyed()))
                location = "file:graphic/" + firstPlayerFields[N][L].getImageType() + "rExp.png";
            else
                location = "file:graphic/hitAndBoom.png";
        }
        Image coverImage = new Image(location);
        if (PLAYER && secondPlayerFields[N][L].isHitted() || !PLAYER && firstPlayerFields[N][L].isHitted())
            oppositeShipsButtons[N][L].setGraphic(new ImageView(coverImage));
    }

    void convertAllTargetFielsdToButton(final boolean PLAYER) {
        for (int n = 0; n < 10; n++) {
            for (int l = 0; l < 10; l++) {
                convertTargetFieldToButton(n, l, PLAYER);
            }
        }
    }

    private void coverAllButtons() {
        for (int n = 0; n < 10; n++)
            for (int l = 0; l < 10; l++) {
                myShipsButtons[n][l].setGraphic(new ImageView(new Image("file:graphic/blank.png")));
                oppositeShipsButtons[n][l].setGraphic(new ImageView(new Image("file:graphic/blank.png")));
            }
    }

    private void updateShipsGrids() {
        myShipsGrid.getChildren().clear();
        oppShipsGrid.getChildren().clear();
        for (int n = 0; n < 10; n++)
            for (int l = 0; l < 10; l++) {
                myShipsGrid.add(myShipsButtons[n][l], n, l);
                oppShipsGrid.add(oppositeShipsButtons[n][l], n, l);
            }
    }

    private boolean checkNeighborhood(final int N, final int L, final int size) {
        boolean flag = true;
        for (int n = N; n < N + size; n++)
            for (int l = L; l < L + size; l++) {
                if ((n < 10 && l < 10)
                        && (orientation && playerTurn && firstPlayerFields[N][l].isSetted()
                        || orientation && !playerTurn && secondPlayerFields[N][l].isSetted()
                        || !orientation && playerTurn && firstPlayerFields[n][L].isSetted()
                        || !orientation && !playerTurn && secondPlayerFields[n][L].isSetted())) {
                    flag = false;
                    break;
                }
            }
        return flag;
    }

    void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    public void setType(char type) {
        this.type = type;
    }

    private void makeMessWindow(String location, Class<?> cls, final int[] WINDOW_PARAMETERS) {
        makeMessWindow(location, cls, WINDOW_PARAMETERS[0], WINDOW_PARAMETERS[1], WINDOW_PARAMETERS[2], WINDOW_PARAMETERS[3]);
    }

    private void makeMessWindow(String location, Class<?> cls, final int HEIGHT, final int WIDTH, final int X, final int Y) {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource(location));
        Parent root;
        Stage messStage = new Stage();
        messStage.setTitle("Waiting window");
        try {
            root = loader.load();
            messStage.setScene(new Scene(root));
        } catch (IOException e) {
            e.printStackTrace();
        }
        messStage.setX(X);
        messStage.setY(Y);
        messStage.setMinWidth(WIDTH);
        messStage.setMaxWidth(WIDTH);
        messStage.setMinHeight(HEIGHT);
        messStage.setMaxHeight(HEIGHT);
        messStage.show();
        if (cls == WaitingController.class) {
            WaitingController waitingController = loader.getController();
            waitingController.setGameBoardController(this);
            waitingController.setNextPlayer(playerTurn);
        } else if (cls == SavingController.class) {
            SavingController savingController = loader.getController();
            savingController.setGameBoardController(this);
            savingController.setIoFileMediatesClass(new IOFileMediatesClass(type, firstPlayerFields, secondPlayerFields, playerTurn, isSetting, isFired, prevN, prevL));
        }
    }

    void setPlayerTurn(boolean playerTurn) {
        this.playerTurn = playerTurn;
    }

    boolean isPlayerTurn() {
        return playerTurn;
    }

    private void addMessage(String message) {
        if (playerTurn || type == 'M')
            messageList.getItems().add("\n" + message);
        else
            messageList.getItems().add("\n AI: " + message);
        ObservableList<String> list = messageList.getItems();
        messageList.scrollTo(list.size() - 1);
    }

    public Label getPlayerLabel() {
        return playerLabel;
    }

    public boolean isSetting() {
        return isSetting;
    }

    private void checkGameStatus() {
        boolean flag = true;
        for (int n = 0; n < 10; n++) {
            for (int l = 0; l < 10; l++) {
                if (secondPlayerFields[n][l].getShip() != null && !secondPlayerFields[n][l].getShip().isDestroyed() && playerTurn) {
                    flag = false;
                }
                if (firstPlayerFields[n][l].getShip() != null && !firstPlayerFields[n][l].getShip().isDestroyed() && !playerTurn) {
                    flag = false;
                }
            }
        }
        if (flag && !isSetting()) {
            isGameEnded = true;
            playerWinLabel.setTextFill(Color.GREEN);
            if (playerTurn)
                playerWinLabel.setText("PLAYER 1 WIN");
            else if (type == 'M')
                playerWinLabel.setText("PLAYER 2 WIN");
            else {
                playerWinLabel.setTextFill(Color.RED);
                playerWinLabel.setText("COMPUTER WIN");
            }
        }
    }

    private void randomAlocate() {
        if (isSetting()) {
            if (playerTurn) {
                while (firstShipsToSetLeft != 0) {
                    int randomN = (int) (Math.random() * 10);
                    int randomL = (int) (Math.random() * 10);
                    int randomOrient = (int) (Math.random() * 2);
                    orientation = (randomOrient % 2 == 0);
                    myShipsEntered(randomN, randomL);
                    myShipsLeftClick();
                    myShipsExited(randomN, randomL);
                    messageList.getItems().clear();
                }
            } else {
                while (secondShipsToSetLeft != 0) {
                    int randomN = (int) (Math.random() * 10);
                    int randomL = (int) (Math.random() * 10);
                    int randomOrient = (int) (Math.random() * 2);
                    orientation = randomOrient % 2 == 0;
                    myShipsEntered(randomN, randomL);
                    myShipsLeftClick();
                    myShipsExited(randomN, randomL);
                    messageList.getItems().clear();
                }
            }
            orientation = true;
            if (type == 'M' || playerTurn)
                addMessage("Random allocating complete!");
            else
                addMessage(" allocating is complete! Hit the target!");
        }
    }

    private void randomHit() {
        int saftyCounter = 0;
        if (!isSetting()) {
            isRandomHitted = false;
            while (!isRandomHitted && !isFired) {
                saftyCounter++;
                if (saftyCounter == 100) {
                    saftyCounter = 0;
                    for (int n = 0; n < 10; n++)
                        for (int l = 0; l < 10; l++) {
                            oppShipsClick(n, l);
                            if (isFired)
                                break;
                        }
                }
                int randomN = (int) (Math.random() * 10);
                int randomL = (int) (Math.random() * 10);
                oppShipsClick(randomN, randomL);
            }
        } else {
            addMessage("Set your ships first!");
        }
    }

    private void easyComputerHit() {
        while (!isFired) {
            randomHit();
        }
    }

    private void regularComputerHit() {
        int saftyCounter = 0;
        while (!isFired) {
            if (!isSetting()) {
                saftyCounter++;
                if (saftyCounter > 100) {
                    prevL.clear();
                    prevN.clear();
                    saftyCounter = 0;
                }
                if (!prevL.isEmpty() && !prevN.isEmpty()) {
                    concentratedHit();
                } else {
                    int tempN = 0;
                    int tempL = 0;
                    isRandomHitted = false;
                    while (!isRandomHitted) {
                        tempN = (int) (Math.random() * 10);
                        tempL = (int) (Math.random() * 10);
                        oppShipsClick(tempN, tempL);
                    }
                    ObservableList<String> list = messageList.getItems();
                    String AIRandomShootMess = list.get(list.size() - 1);
                    if (AIRandomShootMess.compareTo("\n AI: Hit!") == 0) {
                        prevL.add(tempL);
                        prevN.add(tempN);
                    }
                }
            }
        }
    }

    private void concentratedHit() {
        if (prevN.size() == 1) {
            int tempR = (int) (Math.random() * 4);
            if (tempR % 4 == 0) {
                if (prevN.get(0) + 1 < 9) {
                    prevN.add(prevN.get(0) + 1);
                    prevL.add(prevL.get(0));
                }
            } else if (tempR % 4 == 1) {
                if (prevL.get(0) + 1 < 9) {
                    prevN.add(prevN.get(0));
                    prevL.add(prevL.get(0) + 1);
                }
            } else if (tempR % 4 == 2) {
                if (prevN.get(0) - 1 > 0) {
                    prevN.add(prevN.get(0) - 1);
                    prevL.add(prevL.get(0));
                }
            } else {
                if (prevL.get(0) - 1 > 0) {
                    prevN.add(prevN.get(0));
                    prevL.add(prevL.get(0) - 1);
                }
            }
        } else if (prevN.size() > 1) {
            oppShipsClick(prevN.get(prevN.size() - 1), prevL.get(prevL.size() - 1));
            ObservableList<String> list = messageList.getItems();
            String AIRandomShootMess = list.get(list.size() - 1);
            if (AIRandomShootMess.compareTo("\n AI: Hit!") == 0) {
                prevL.add(prevL.get(prevL.size() - 1) + (prevL.get(prevL.size() - 1) - prevL.get(prevL.size() - 2)));
                prevN.add(prevN.get(prevN.size() - 1) + (prevN.get(prevN.size() - 1) - prevN.get(prevN.size() - 2)));
                if (prevL.get(prevL.size() - 1) > 9 || prevL.get(prevL.size() - 1) < 0 || prevN.get(prevN.size() - 1) > 9 || prevN.get(prevN.size() - 1) < 0) {
                    int tempL = prevL.get(0);
                    int tempN = prevN.get(0);
                    prevL.clear();
                    prevN.clear();
                    prevL.add(tempL);
                    prevN.add(tempN);
                }
            } else if ((AIRandomShootMess.compareTo("\n AI: Miss.. ") == 0 || AIRandomShootMess.compareTo("\n AI: Uncorrected target!") == 0)) {
                int prevSize = prevN.size();
                int tempL = prevL.get(0);
                int tempN = prevN.get(0);
                int tempL2 = 2 * prevL.get(0) - prevL.get(1);
                int tempN2 = 2 * prevN.get(0) - prevN.get(1);
                prevL.clear();
                prevN.clear();
                prevL.add(tempL);
                prevN.add(tempN);
                if (prevSize > 2) {
                    prevL.add(tempL2);
                    prevN.add(tempN2);
                    if (tempL2 > 9 || tempL2 < 0 || tempN2 > 9 || tempN2 < 0) {
                        prevL.clear();
                        prevN.clear();
                    }
                }
            } else {
                prevL.clear();
                prevN.clear();
            }
        }
    }

    private void hardComputerHit() {
        int randomToHundred = (int) (Math.random() * 100);
        if (HARD_AI_PERCENTAGE_CHANCE_TO_AUTOHIT - randomToHundred >= 0 && prevN.isEmpty()) {
            for (int n = 0; n < 10; n++)
                for (int l = 0; l < 10; l++) {
                    if (firstPlayerFields[n][l].getShip() != null && !firstPlayerFields[n][l].isHitted()) {
                        prevN.add(n);
                        prevL.add(l);
                        oppShipsClick(n, l);
                        break;
                    }
                }
        } else {
            regularComputerHit();
        }
    }

    void load(IOFileMediatesClass ioFileMediatesClass) {
        type = ioFileMediatesClass.getType();
        firstPlayerFields = ioFileMediatesClass.getFirstShipsFieldArray();
        secondPlayerFields = ioFileMediatesClass.getSecondShipsFieldArray();
        playerTurn = ioFileMediatesClass.isPlayerTurn();
        isSetting = ioFileMediatesClass.isSetting();
        isFired = ioFileMediatesClass.isShooted();
        prevN = ioFileMediatesClass.getPrevN();
        prevL = ioFileMediatesClass.getPrevL();
        isSetting = false;
        firstShipsToSetLeft = 0;
        secondShipsToSetLeft = 0;
        rotLabel.setText("");
        setPlayerTurn(!isPlayerTurn());
        waitTheGame();
    }
}
