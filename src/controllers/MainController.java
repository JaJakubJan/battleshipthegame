package controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import main.IOFileMediatesClass;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable, Serializable {
    final static String MENU_SCREEN_LOCATION        = "/fxml/MenuScreen.fxml";
    final static String GAMEBOARD_SCREEN_LOCATION   = "/fxml/GameBoardScreen.fxml";
    final static String GAME_OPTION_SCREEN_LOCATION = "/fxml/GameOptionScreen.fxml";
    final static String GAME_LOAD_SCREEN_LOCATION   = "/fxml/LoadGameScreen.fxml";
    final static String SAVING_SCREEN_LOCATION      ="/fxml/SavingScreen.fxml";
    final static String SAVING_FOLDER_LOCATION      = "saves/";
    final static String SAVING_FORMAT               = ".bsv";
    final static int[] SMALL_WINDOW_PARAMETERS      = {330, 240, 800, 300}; // height, width, x, y;
    final static int[] BIG_WINDOW_PARAMETERS        = {700, 1100, 400, 250}; // height, width, x, y;

    @FXML
    private StackPane mainStackPane;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        setMenu();
    }

    void resizeAndReallocWindow(final int[] WINDOW_PARAMETERS){
        resizeAndReallocWindow(WINDOW_PARAMETERS[0], WINDOW_PARAMETERS[1], WINDOW_PARAMETERS[2], WINDOW_PARAMETERS[3]);
    }

    void resizeAndReallocWindow(final int HEIGHT, final int WIDTH, final int X, final int Y) {
        Stage mainStage = (Stage) mainStackPane.getScene().getWindow();
        mainStage.setX(X);
        mainStage.setY(Y);
        mainStage.setMinWidth(WIDTH);
        mainStage.setMaxWidth(WIDTH);
        mainStage.setMinHeight(HEIGHT);
        mainStage.setMaxHeight(HEIGHT);
    }

    void setPane(String localization, Class<?> cls) {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource(localization));
        Pane nextPane = null;
        try {
            nextPane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mainStackPane.getChildren().clear();
        if (cls == MenuController.class) {
            MenuController menuController = loader.getController();
            menuController.setMainController(this);
        } else if (cls == GameOptionController.class) {
            GameOptionController gameOptionController = loader.getController();
            gameOptionController.setMainController(this);
        } else if (cls == LoadGameController.class) {
            LoadGameController loadGameController = loader.getController();
            loadGameController.setMainController(this);
        }
        mainStackPane.getChildren().add(nextPane);
    }

    void setBoardGame(char Type) {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource(GAMEBOARD_SCREEN_LOCATION));
        Pane nextPane = null;
        try {
            nextPane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mainStackPane.getChildren().clear();
        GameBoardController gameBoardController = loader.getController();
        gameBoardController.setType(Type);
        gameBoardController.setMainController(this);
        mainStackPane.getChildren().add(nextPane);
    }

    void setBoardGame(IOFileMediatesClass ioFileMediatesClass) {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource(GAMEBOARD_SCREEN_LOCATION));
        Pane nextPane = null;
        try {
            nextPane = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mainStackPane.getChildren().clear();
        GameBoardController gameBoardController = loader.getController();
        gameBoardController.setMainController(this);
        mainStackPane.getChildren().add(nextPane);
        gameBoardController.load(ioFileMediatesClass);
    }

    void setMenu() {
        setPane(MENU_SCREEN_LOCATION, MenuController.class);
    }
}
