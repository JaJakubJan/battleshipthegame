package controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import main.IOFileMediatesClass;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.net.URL;
import java.util.ResourceBundle;

public class LoadGameController implements Initializable {
    private MainController mainController;

    @FXML
    private ChoiceBox loadChoiceBox;
    @FXML
    private Label loadingErrorLabel;

    @FXML
    private void loadOnAction() {
        if (!loadChoiceBox.getItems().isEmpty()) {
            String location = MainController.SAVING_FOLDER_LOCATION + loadChoiceBox.getValue().toString() + MainController.SAVING_FORMAT;
            try {
                ObjectInputStream os = new ObjectInputStream(new FileInputStream(location));
                IOFileMediatesClass ioFileMediatesClass = (IOFileMediatesClass) os.readObject();
                mainController.setBoardGame(ioFileMediatesClass);
                mainController.resizeAndReallocWindow(MainController.BIG_WINDOW_PARAMETERS);
            } catch (Exception ex) {
                loadingErrorLabel.setText("Loading error!");
            }
        } else
            loadingErrorLabel.setText("No saves to load!");
    }

    @FXML
    private void backOnAction() {
        mainController.setMenu();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        loadingErrorLabel.setText("");
        try {
            File folder = new File(MainController.SAVING_FOLDER_LOCATION);
            if (!folder.isDirectory() || !folder.exists()) folder.mkdir();
            File[] listOfFiles = folder.listFiles();
            for (File tempFile : listOfFiles)
                if (tempFile.isFile())
                    loadChoiceBox.getItems().add(tempFile.getName().substring(0, (tempFile.getName().length() - MainController.SAVING_FORMAT.length())));
            loadChoiceBox.getSelectionModel().select(0);
        } catch (Exception ex) {
            loadingErrorLabel.setText("Loading error!");
        }
    }

    void setMainController(MainController mainController) {
        this.mainController = mainController;
    }
}
