package controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import main.IOFileMediatesClass;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.ResourceBundle;

public class SavingController implements Initializable {
    private GameBoardController gameBoardController;
    private IOFileMediatesClass ioFileMediatesClass;

    @FXML
    private TextField saveTextField;
    @FXML
    private Label messageLabel;

    @FXML
    private void saveOnAction() {
        boolean flag = true;
        File folder = new File(MainController.SAVING_FOLDER_LOCATION);
        if (!folder.isDirectory() || !folder.exists())
            folder.mkdir();
        try {
            FileOutputStream fs = new FileOutputStream(MainController.SAVING_FOLDER_LOCATION + saveTextField.getText() + MainController.SAVING_FORMAT);
            ObjectOutputStream os = new ObjectOutputStream(fs);
            os.writeObject(ioFileMediatesClass);
            os.close();
        } catch (Exception ex) {
            messageLabel.setTextFill(Color.RED);
            messageLabel.setText("Saving error!");
            flag = false;
            ex.printStackTrace();
        }
        if (flag) {
            messageLabel.setTextFill(Color.GREEN);
            messageLabel.setText("Saving successful!");
        }
    }

    @FXML
    private void quitOnAction() {
        Platform.exit();
    }

    @FXML
    private void backOnAction() {
        Stage gameStage = (Stage) gameBoardController.getPlayerLabel().getScene().getWindow();
        gameStage.show();
        Stage stage = (Stage) saveTextField.getScene().getWindow();
        stage.close();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        messageLabel.setText("");
    }

    void setGameBoardController(GameBoardController gameBoardController) {
        this.gameBoardController = gameBoardController;
    }

    void setIoFileMediatesClass(IOFileMediatesClass ioFileMediatesClass) {
        this.ioFileMediatesClass = ioFileMediatesClass;
    }
}
