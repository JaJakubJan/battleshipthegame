package controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import java.net.URL;
import java.util.ResourceBundle;

public class MenuController implements Initializable {
    private MainController mainController;

    @FXML
    private ImageView logoImage;

    @FXML
    private void newGameOnAction() {
        mainController.setPane(MainController.GAME_OPTION_SCREEN_LOCATION, GameOptionController.class);
    }

    @FXML
    private void loadGameOnAction() {
        mainController.setPane(MainController.GAME_LOAD_SCREEN_LOCATION, LoadGameController.class);
    }

    @FXML
    private void quitOnAction() {
        Platform.exit();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        logoImage.setImage(new Image("file:graphic/logo.png"));
    }

    void setMainController(MainController mainController) {
        this.mainController = mainController;
    }
}
