package controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class WaitingController implements Initializable {
    private GameBoardController gameBoardController;

    @FXML
    private Label playerLabel;

    @FXML
    public void readyOnAction() {
        gameBoardController.setPlayerTurn(!gameBoardController.isPlayerTurn());
        gameBoardController.convertAllVisableFielsdToButton(gameBoardController.isPlayerTurn());
        gameBoardController.convertAllTargetFielsdToButton(gameBoardController.isPlayerTurn());
        Stage gameStage = (Stage) gameBoardController.getPlayerLabel().getScene().getWindow();
        gameStage.show();
        Stage stage = (Stage) playerLabel.getScene().getWindow();
        stage.close();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }

    void setGameBoardController(GameBoardController gameBoardController) {
        this.gameBoardController = gameBoardController;
    }

    void setNextPlayer(boolean playerTurn) {
        String communicateText = playerTurn ? "to Player 2" : "to Player 1";
        playerLabel.setText(communicateText);
    }
}
