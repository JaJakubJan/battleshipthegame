package main;

import java.io.Serializable;

public class Ship implements Serializable {
    private String name;
    private int bulk;

    public Ship(String name, int bulk) {
        this.name = name;
        this.bulk = bulk;
    }

    private void setBulk(int bulk) {
        this.bulk = bulk;
    }

    void hit() {
        setBulk(--bulk);
    }

    public String getName() {
        return name;
    }

    public boolean isDestroyed() {
        return (bulk == 0);
    }

    public int getBulk() {
        return bulk;
    }
}
