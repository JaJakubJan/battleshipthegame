/*
This class is optional
Every X and Y parameters should be change to Coordinate class to make GameController code more clearly
Its a first step to upgrade BattleshipTheGame
*/

package main;
import java.io.Serializable;

public class Coordinate implements Serializable {
    public static final int MAXIMAL_ROW = 10;
    public static final int MINIMAL_ROW = 0;
    public static final int MAXIMAL_COLUMN = 10;
    public static final int MINIMAL_COLUMN = 0;
    private final int row;
    private final int column;

    public static Coordinate FromParameters(int row, int column){
        return new Coordinate(row, column);
    }

    public static Coordinate FromRandomValues(){
        int randomRow =  (int) (MINIMAL_ROW + Math.random() * (MAXIMAL_ROW - MINIMAL_ROW + 1));
        int randomColumn =  (int) (MINIMAL_COLUMN + Math.random() * (MAXIMAL_COLUMN - MINIMAL_COLUMN + 1));
        return new Coordinate(randomRow, randomColumn);
    }

    private Coordinate(int row, int column){
        this.row = row;
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public boolean isRowOutOfRange(){
        return row > MAXIMAL_ROW || row < MINIMAL_ROW;
    }

    public boolean isColumnOutOfRange(){
        return column > MAXIMAL_COLUMN || column < MINIMAL_COLUMN;
    }

    public boolean isOutOfRange(){
        return isRowOutOfRange() || isColumnOutOfRange();
    }
}
