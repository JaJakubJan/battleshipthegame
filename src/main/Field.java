package main;

import java.io.Serializable;

public class Field implements Serializable {
    private String imageType = "blank";
    private boolean isHitted = false;
    private boolean isSetted = false;
    private Ship ship;
    private Coordinate COORDINATE;

    public Field(){
        this.COORDINATE = Coordinate.FromRandomValues();
    }

    public Field(int row, int column){
        this.COORDINATE = Coordinate.FromParameters(row, column);
    }

    public String hit() {
        if (!isHitted()) {
            setHitted(true);
            if (ship != null) {
                ship.hit();
                if (ship.isDestroyed()) {
                    return "Enemy " + ship.getName() + " is destroyed!";
                } else {
                    return "Hit!";
                }
            }
            return "Miss.. ";
        }
        return "Uncorrected target!";
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public void setImageType(String imageType, Ship ship) {
        setShip(ship);
        this.imageType = imageType;
    }

    public boolean isHitted() {
        return isHitted;
    }

    public void setHitted(boolean hitted) {
        isHitted = hitted;
    }

    public boolean isSetted() {
        return isSetted;
    }

    public void setSetted(boolean setted) {
        isSetted = setted;
    }

    public Ship getShip() {
        return ship;
    }

    public void setShip(Ship ship) {
        this.ship = ship;
    }
}
