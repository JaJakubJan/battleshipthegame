package main;

import java.io.Serializable;
import java.util.ArrayList;

public class IOFileMediatesClass implements Serializable {
    private char type;
    private Field[][] firstShipsFieldArray;
    private Field[][] secondShipsFieldArray;
    private boolean playerTurn;
    private boolean isSetting;
    private boolean isShooted;
    ArrayList<Integer> prevN;
    ArrayList<Integer> prevL;

    public IOFileMediatesClass(char type, Field[][] firstShipsFieldArray, Field[][] secondShipsFieldArray, boolean playerTurn, boolean isSetting, boolean isShooted, ArrayList<Integer> prevN, ArrayList<Integer> prevL) {
        this.type = type;
        this.firstShipsFieldArray = firstShipsFieldArray;
        this.secondShipsFieldArray = secondShipsFieldArray;
        this.playerTurn = playerTurn;
        this.isSetting = isSetting;
        this.isShooted = isShooted;
        this.prevN = prevN;
        this.prevL = prevL;
    }

    public char getType() {
        return type;
    }

    public Field[][] getFirstShipsFieldArray() {
        return firstShipsFieldArray;
    }

    public Field[][] getSecondShipsFieldArray() {
        return secondShipsFieldArray;
    }

    public boolean isPlayerTurn() {
        return playerTurn;
    }

    public boolean isSetting() {
        return isSetting;
    }

    public boolean isShooted() {
        return isShooted;
    }

    public ArrayList<Integer> getPrevN() {
        return prevN;
    }

    public ArrayList<Integer> getPrevL() {
        return prevL;
    }

}
