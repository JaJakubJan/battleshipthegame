module BattleshipsTheGame {
    requires javafx.controls;
    requires javafx.fxml;

    opens main;
    opens controllers;
}